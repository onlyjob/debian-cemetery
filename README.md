Abandoned Debian packaging for various software (left after evaluation
etc). Not worth including to [Debian](http://debian.org) usually due to
DFSG concerns (found during packaging) or because software is not good
enough.
