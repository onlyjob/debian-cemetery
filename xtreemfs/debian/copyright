Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: XtreemFS
Source: http://www.xtreemfs.org
Files-Excluded: cpp/thirdparty/gtest-*
                cpp/thirdparty/protobuf-*
                java/lib/commons-codec-*.jar
                java/lib/protobuf-java-*.jar
                java/lib/test/junit-*.jar
                cpp/include/json
                cpp/src/json

Files: *
Copyright: 2008-2013 Zuse Institute Berlin
                     Björn Kolbeck
                     Christian Lorenz
                     Eugenio Cesario
                     Felix Langner
                     Jan Stender
                     Johannes Dillmann
                     Juan Gonzalez
                     Matthias Noack
                     Michael Berlin
                     Patrick Schäfer
                     Paul Seiferth
License: BSD-3-clause

Files: debian/*
Copyright: 2014      Dmitry Smirnov <onlyjob@debian.org>
           2008-2013 Zuse Institute Berlin
                     Björn Kolbeck
                     Christian Lorenz
                     Eugenio Cesario
                     Felix Langner
                     Jan Stender
                     Johannes Dillmann
                     Juan Gonzalez
                     Matthias Noack
                     Michael Berlin
                     Patrick Schäfer
                     Paul Seiferth
License: BSD-3-clause

License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
    * Redistributions of source code must retain the above
      copyright notice, this list of conditions and the
      following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials
      provided with the distribution.
    * Neither the name of the Zuse Institute Berlin nor the
      names of its contributors may be used to endorse or promote
      products derived from this software without specific prior
      written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
